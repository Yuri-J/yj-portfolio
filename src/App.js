import "./App.css";
import React from "react";

import CustomSwitch from "./switch";
import navBarWithSwitch from "./sections/NavBar";

const App = () => {
  return (
    <>
      <div className="App">{navBarWithSwitch(CustomSwitch)}</div>
    </>
  );
};

export default App;

import React from "react";
import _ from "lodash";

import styled from "styled-components";
import { MDBContainer, MDBRow, MDBCol, MDBCardTitle } from "mdbreact";

import Laptop from "../assets/laptop.png";
import SmartPhone from "../assets/smartPhone.png";

import ReactIcon from "../assets/react.svg";
import VueIcon from "../assets/vue.svg";
import JsIcon from "../assets/javascript.svg";
import HtmlIcon from "../assets/html.svg";
import SassIcon from "../assets/sass.svg";
import PhpIcon from "../assets/php.svg";

import NodeJsIcon from "../assets/nodejs.svg";
import MongoDbIcon from "../assets/mongo.svg";
import MySqlIcon from "../assets/mysql.svg";
import PhotoshopIcon from "../assets/photoshop.svg";
import FigmaIcon from "../assets/figma.svg";

const MDBContainerS = styled(MDBContainer)`
  background: url(${Laptop}) no-repeat;
  background-size: contain;
  background-position: center;
  min-height: 90vh;
  @media (max-width: 992px) {
    min-height: 70vh;
  }
  @media (max-width: 576px) {
    min-height: 66vh;

    background: url(${SmartPhone}) no-repeat;
    background-size: contain;
    background-position: center;
  }
`;

const H1 = styled(MDBCardTitle)`
  @media (min-width: 992px) {
    padding-top: 4rem;
  }
  @media (min-width: 768px) and (max-width: 992px) {
    padding-top: 4rem;
  }
  @media (min-width: 576px) and (max-width: 768px) {
    padding-top: 6rem;
  }
  @media (max-width: 576px) {
    padding-bottom: 5rem;
    position: relative;
    top: 4rem;
  }
`;

const DivBox = styled.div`
  padding-bottom: 2rem;
  margin: 0 10rem;

  @media (min-width: 992px) and (max-width: 1200px) {
    margin: 0 6rem;
  }
  @media (min-width: 768px) and (max-width: 992px) {
    margin: 0 5.8rem;
  }
  @media (min-width: 576px) and (max-width: 768px) {
    margin: 0 5rem;
  }
  @media (max-width: 576px) {
    margin: 0 11rem;
  }
  @media (max-width: 540px) {
    margin: 0 10rem;
  }
  @media (max-width: 507px) {
    margin: 0 9rem;
  }
  @media (max-width: 476px) {
    margin: 0 8rem;
  }
  @media (max-width: 444px) {
    margin: 0 7rem;
  }
  @media (max-width: 414px) {
    margin: 0 6rem;
  }
  @media (max-width: 380px) {
    margin: 0 5rem;
  }
`;

const Div = styled.div`
  @media (min-width: 1200px) {
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    width: 6rem;
    height: 6rem;
    border-radius: 50%;
    background-color: #eaeaea;
  }

  @media (min-width: 992px) and (max-width: 1200px) {
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    width: 5.6rem;
    height: 5.6rem;
    border-radius: 50%;
    background-color: #eaeaea;
  }
  @media (min-width: 768px) and (max-width: 992px) {
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    width: 3.6rem;
    height: 3.6rem;
    border-radius: 50%;
    background-color: #eaeaea;
  }
  @media (min-width: 576px) and (max-width: 768px) {
    // position: relative;
    // top: 4rem;
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    width: 3.6rem;
    height: 3.6rem;
    border-radius: 50%;
    background-color: #eaeaea;
  }
  @media (max-width: 576px) {
    position: relative;
    bottom: 0rem;
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    width: 3.6rem;
    height: 3.6rem;
    border-radius: 50%;
    background-color: #eaeaea;
    padding: 0;
  }
`;

const MDBColS = styled(MDBCol)`
  @media (max-width: 768px) {
    padding: 0;
  }
`;

const iconItems = [
  {
    img: ReactIcon,
  },
  {
    img: VueIcon,
  },
  {
    img: JsIcon,
  },
  {
    img: HtmlIcon,
  },
  {
    img: SassIcon,
  },
  {
    img: NodeJsIcon,
  },
  {
    img: MongoDbIcon,
  },
  {
    img: MySqlIcon,
  },
  {
    img: PhotoshopIcon,
  },
  {
    img: FigmaIcon,
  },
  {
    img: PhpIcon,
  },
  {
    img: PhpIcon,
  },
];

const Skills = () => {
  return (
    <>
      <MDBContainerS id="skills" className="">
        <DivBox>
          <H1 className=" h1-responsive text-center font-weight-bold">
            MY SKILLS
          </H1>
          <MDBRow>
            {_.map(iconItems, (item, index) => (
              <MDBColS key={index}>
                <Div className="m-2 d-flex justify-content-center mx-auto">
                  <img src={item.img} alt="" className="w-75" />
                </Div>
              </MDBColS>
            ))}
          </MDBRow>
        </DivBox>
      </MDBContainerS>
    </>
  );
};

export default Skills;

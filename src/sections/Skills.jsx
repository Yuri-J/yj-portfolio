import React from "react";
import _ from "lodash";

import styled from "styled-components";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCardTitle,
  MDBView,
  MDBMask,
} from "mdbreact";

// import Bg from "../assets/bg-skill3.png";
import Laptop from "../assets/laptop.png";

import ReactIcon from "../assets/react.svg";
import VueIcon from "../assets/vue.svg";
import JsIcon from "../assets/javascript.svg";
import HtmlIcon from "../assets/html.svg";
import SassIcon from "../assets/sass.svg";
import PhpIcon from "../assets/php.svg";

import NodeJsIcon from "../assets/nodejs.svg";
import MongoDbIcon from "../assets/mongo.svg";
import MySqlIcon from "../assets/mysql.svg";
import PhotoshopIcon from "../assets/photoshop.svg";
import FigmaIcon from "../assets/figma.svg";

const MDBContainerS = styled(MDBContainer)`
  background: url(${Laptop}) no-repeat;
  background-size: contain;
  background-position: center;
`;

const MDBCardTitleS = styled(MDBCardTitle)`
  // position: relative;
  // bottom: 1.8rem;
`;

const MDBColS = styled(MDBContainer)`
  // padding: 0 5rem;
  // background: #f5e3d8;

  // min-height: 60vh;
  // box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px,
  //   rgba(0, 0, 0, 0.22) 0px 15px 12px;
`;

const DivBox = styled.div`
  padding-bottom: 2rem;
  @media (min-width: 992px) {
    margin: 0 10rem;
  }
`;

const Div = styled.div`
  border: 4px dashed pink;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  width: 4rem;
  height: 4rem;
  border-radius: 50%;
  background-color: #eaeaea;
`;

const iconItems = [
  {
    img: ReactIcon,
  },
  {
    img: VueIcon,
  },
  {
    img: JsIcon,
  },
  {
    img: HtmlIcon,
  },
  {
    img: SassIcon,
  },
  {
    img: NodeJsIcon,
  },
  {
    img: MongoDbIcon,
  },
  {
    img: MySqlIcon,
  },
  {
    img: PhotoshopIcon,
  },
  {
    img: FigmaIcon,
  },
  {
    img: PhpIcon,
  },
  {
    img: PhpIcon,
  },
];

const Skills = () => {
  return (
    <>
      <MDBView>
        <MDBContainerS id="skills" className="pb-5" fluid>
          <MDBColS>
            <DivBox>
              <MDBCardTitleS className="pt-3 h1-responsive text-center font-weight-bold">
                MY SKILLS
              </MDBCardTitleS>
              <MDBRow>
                {_.map(iconItems, (item, index) => (
                  <MDBCol key={index}>
                    <div className="text-center m-3 d-flex justify-content-center mx-auto">
                      <Div>
                        <img src={item.img} alt="" className="w-100" />
                      </Div>
                    </div>
                  </MDBCol>
                ))}
              </MDBRow>
            </DivBox>
          </MDBColS>
        </MDBContainerS>
      </MDBView>
    </>
  );
};

export default Skills;

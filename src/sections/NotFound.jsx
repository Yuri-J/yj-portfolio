import React from "react";

import { MDBContainer, MDBRow, MDBCol } from "mdbreact";

import SadDog from "../assets/saddog.png";

const NotFound = () => {
  return (
    <>
      <MDBContainer>
        <MDBRow>
          <MDBCol sm="6">
            <img src={SadDog} alt="" className="w-100" />
          </MDBCol>
          <MDBCol sm="6">
            <div class="cloud">
              <h1 className="h1-responsive p-5 text-center">
                Sorry Hooman,
                <br />
                <span className="font-weight-bold">Page not found.</span>
              </h1>
            </div>
          </MDBCol>
        </MDBRow>
        <div className="text-center"></div>
      </MDBContainer>
    </>
  );
};

export default NotFound;

import React from "react";
import styled from "styled-components";

import { MDBContainer } from "mdbreact";

import PortlaitR from "../assets/portlait-r.jpg";
import JumboBg from "../assets/jumbo-bg.jpg";

const MDBContainerS = styled(MDBContainer)`
  background: #f5e3d8 url(${JumboBg}) no-repeat;
  background-size: cover;

  @media (max-width: 992px) {
    padding-bottom: 10rem;
  }
`;

const DivImg = styled.div`
  @media (min-width: 992px) {
    width: 90%;
  }
  @media (max-width: 992px) {
    position: relative;
    top: 6rem;
  }
`;

const JumboContainer = styled.div`
  width: 100%;
  position: relative;
`;

const Textbox = styled.div`
  color: rgb(53, 50, 50);
  position: absolute;
`;

const H1Title = styled.h1`
  white-space: nowrap;
  @media (max-width: 992px) {
    position: relative;
    z-index: 1;
  }
`;

const H1SubTitle = styled.h1`
  @media (max-width: 992px) {
    padding-bottom: 3rem;
    position: relative;
    z-index: 1;
  }
`;

const H4 = styled.h4`
  @media (max-width: 992px) {
    display: none;
  }
`;

const Jumbo = () => {
  return (
    <>
      <MDBContainerS fluid style={{ paddingBottom: "12rem" }}>
        <MDBContainer className="py-5">
          <JumboContainer className="d-flex flex-column justify-content-center">
            <Textbox className="m-5">
              <H1Title className="h1-responsive font-weight-bold my-3">
                HELLO, I’m Yuri J
              </H1Title>
              <H1SubTitle className="h1-responsive font-weight-bold">
                Junior Web Developer based in Melbourne
              </H1SubTitle>
              <H4 className="mt-5">
                I’m a highly flexible and adaptable when it comes to different
                projects.
              </H4>
            </Textbox>
            <DivImg className="text-right">
              <img
                src={PortlaitR}
                className="img-fluid w-75"
                style={{ boxShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px" }}
                alt=""
              />
            </DivImg>
          </JumboContainer>
        </MDBContainer>
      </MDBContainerS>
    </>
  );
};

export default Jumbo;

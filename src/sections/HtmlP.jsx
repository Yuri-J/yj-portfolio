import React from "react";
import _ from "lodash";
import styled from "styled-components";
import { MDBContainer, MDBView, MDBMask, MDBRow, MDBCol } from "mdbreact";

import Html1 from "../assets/html-1.png";
import Html2 from "../assets/html-2.png";

const MDBContainerS = styled(MDBContainer)`
  background: linear-gradient(
    180deg,
    #5e6f64 0%,
    #5e6f64 220px,
    #f5e3d8 100px,
    #f5e3d8 100%
  );
`;

const DivBorder = styled.div`
  border: 8px solid white;
`;

const H5 = styled.h5`
  padding: 1.2rem 0;
  font-weight: bold;
  text-align: center;
  color: #484848;
`;

const htmlItems = [
  {
    title: "Impossible Triangle",
    desc: "Simple HTML & CSS project.",
    url: "https://yuri-j.gitlab.io/assignment-impossible-triangle/",
    img: Html1,
  },
  {
    title: "Shiki Furniture",
    desc: "Simple HTML & CSS project.",
    url: "https://yuri-j.gitlab.io/assignment-shiki-furniture/",
    img: Html2,
  },
];

const HtmlP = () => {
  return (
    <>
      <MDBContainerS fluid>
        <MDBContainer>
          <h2 className="font-weight-bold pt-5 mx-4 text-white">HTML</h2>
          <MDBRow className="p-4 d-flex justify-content-start">
            {_.map(htmlItems, (item, index) => (
              <MDBCol key={index} lg="4" md="6" sm="6">
                <DivBorder>
                  <MDBView hover>
                    <a
                      href={item.url}
                      target="_blank"
                      rel="noreferrer noopener"
                    >
                      <img
                        src={item.img}
                        className="mx-auto img-fluid w-100"
                        alt=""
                      />
                      <MDBMask className="flex-center" overlay="stylish-strong">
                        <p className="white-text text-center">{item.desc}</p>
                      </MDBMask>
                    </a>
                  </MDBView>
                </DivBorder>
                <H5>{item.title}</H5>
              </MDBCol>
            ))}
          </MDBRow>
        </MDBContainer>
      </MDBContainerS>
    </>
  );
};

export default HtmlP;

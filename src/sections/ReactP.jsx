import React from "react";
import _ from "lodash";

import styled from "styled-components";
import { MDBContainer, MDBView, MDBMask, MDBRow, MDBCol } from "mdbreact";

import React1 from "../assets/react-1.png";
import React2 from "../assets/react-2.png";
import React3 from "../assets/react-3.png";
import React4 from "../assets/react-4.png";
import React5 from "../assets/react-5.png";
import React6 from "../assets/react-6.png";

const MDBContainerS = styled(MDBContainer)`
  background: #ba7967;
`;

const DivBorder = styled.div`
  border: 8px solid white;
`;
const H1 = styled.h1`
  position: relative;
  bottom: 1.5rem;
`;

const reactItems = [
  {
    title: "Diary Manager",
    desc: "Inplemented with Mongodb and Heroku. ",
    url: "https://yurij2020.github.io/diary-manager-ui/",
    img: React1,
  },
  {
    title: "Bewdy Organics",
    desc: "EC Cosmetic webside with Redux toolkit",
    url: "https://yuri-j.gitlab.io/assignment-bewdy-organics/",
    img: React2,
  },
  {
    title: "Studio Ghibli Movie Collection",
    desc: "Movie Database.",
    url: "https://yuri-j.gitlab.io/assignment-studio-ghibli-collection/",
    img: React3,
  },
  {
    title: "Picsum Viewer",
    desc: "Image viewer by Picsum with Redux toolkit.",
    url: "https://yuri-j.gitlab.io/assignment-picsum-viewer/",
    img: React4,
  },
  {
    title: "Recipe Binder",
    desc: "Recipe binder app with redux hosted by firebase.",
    url: "https://recipe-binder-react.web.app/",
    img: React5,
  },
  {
    title: "Hair Salon Eureka",
    desc: "Hair salon website.",
    url: "https://yuri-j.gitlab.io/hair-salon-demo/homepage",
    img: React6,
  },
];

const ReactP = () => {
  return (
    <>
      <MDBContainerS id="portfolio" fluid>
        <MDBContainer>
          <H1 className="h1-responsive font-weight-bold">PORTFOLIO</H1>
          <h2 className="h2-responsive font-weight-bold text-center pb-4 text-white">
            React.js
          </h2>
          <MDBRow className="text-white text-center p-4">
            {_.map(reactItems, (item, index) => (
              <MDBCol key={index} lg="4" md="6" sm="6" className="mb-4">
                <DivBorder>
                  <MDBView hover>
                    <a
                      href={item.url}
                      target="_blank"
                      rel="noreferrer noopener"
                    >
                      <img
                        src={item.img}
                        className="mx-auto img-fluid w-100"
                        alt=""
                      />
                      <MDBMask className="flex-center" overlay="stylish-strong">
                        <p className="white-text text-center px-3">
                          {item.desc}
                        </p>
                      </MDBMask>
                    </a>
                  </MDBView>
                </DivBorder>
                <h5 className="py-3 font-weight-bold">{item.title}</h5>
              </MDBCol>
            ))}
          </MDBRow>
        </MDBContainer>
      </MDBContainerS>
    </>
  );
};

export default ReactP;

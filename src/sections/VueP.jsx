import React from "react";
import _ from "lodash";

import styled from "styled-components";
import { MDBContainer, MDBView, MDBMask, MDBRow, MDBCol } from "mdbreact";

import Vue1 from "../assets/vue-1.png";
import Vue2 from "../assets/vue-2.png";

const MDBContainerS = styled(MDBContainer)`
  background: linear-gradient(
    180deg,
    #f5e3d8 0%,
    #f5e3d8 220px,
    #5e6f64 100px,
    #5e6f64 100%
  );
`;

const DivBorder = styled.div`
  border: 8px solid white;
`;

const H5 = styled.h5`
  padding: 1.2rem 0;
  font-weight: bold;
  text-align: center;
  color: white;
`;

const vueItems = [
  {
    title: "Llama Yoga",
    desc: "Yoga website for the Internship program. Backend hosted by wordpress.",
    url: "https://llamayoga.com.au/",
    img: Vue1,
  },
  {
    title: "Recipe Binder",
    desc: "Recipe Binder. Backend hosted by Heroku.",
    url: "https://recipe-binder-vue.herokuapp.com/",
    img: Vue2,
  },
];

const VueP = () => {
  return (
    <>
      <MDBContainerS fluid>
        <MDBContainer>
          <h2 className="font-weight-bold pt-5 text-right mx-5">Vue.js</h2>
          <MDBRow className="p-4 d-flex justify-content-end">
            {_.map(vueItems, (item, index) => (
              <MDBCol key={index} lg="4" md="6" sm="6">
                <DivBorder>
                  <MDBView hover>
                    <a
                      href={item.url}
                      target="_blank"
                      rel="noreferrer noopener"
                    >
                      <img src={item.img} className=" img-fluid w-100" alt="" />
                      <MDBMask className="flex-center" overlay="stylish-strong">
                        <p className="white-text text-center px-3">
                          {item.desc}
                        </p>
                      </MDBMask>
                    </a>
                  </MDBView>
                </DivBorder>
                <H5>{item.title}</H5>
              </MDBCol>
            ))}
          </MDBRow>
        </MDBContainer>
      </MDBContainerS>
    </>
  );
};

export default VueP;

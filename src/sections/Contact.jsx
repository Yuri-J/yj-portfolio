import React from "react";
// import _ from "lodash";

import styled from "styled-components";

import { MDBContainer, MDBRow, MDBCol } from "mdbreact";

const FormS = styled.form`
  @media (min-width: 992px) {
    padding: 0 4rem;
  }
`;

const H2 = styled.h2`
  @media (max-width: 768px) {
    text-align: center;
  }
`;

const MDBColS = styled(MDBCol)`
  @media (max-width: 768px) {
    display: none;
  }
`;

const Div = styled.div`
  padding-top: 1rem;
  @media (max-width: 768px) {
    text-align: center;
  }
`;

const Input = styled.input`
display: inline-block;
padding: 0.8em 3em;
text-decoration: none;
color: white;
box-shadow: 0 2px 4px rgba(0,0,0,0.3);
background-color: #64759c;
border: solid 2px #64759c;
transition: 0.4s;

&:hover {
background-color: #525f80;
border: solid 2px #525f80;
}
}
`;

const Contact = () => {
  return (
    <>
      <MDBContainer id="contact" fluid style={{ backgroundColor: "#ba7967" }}>
        <FormS className="py-4">
          {/* onSubmit={handleSubmit(sendEmail)} ref={formRef} */}
          <MDBRow>
            <MDBCol md="4">
              <H2 className="text-white mx-5 font-weight-bold">CONTACT</H2>
            </MDBCol>

            <MDBCol md="4">
              <label htmlFor="defaultFormLoginEmailEx" className="white-text">
                Your Name
              </label>
              <input
                id="defaultFormLoginEmailEx"
                className="form-control"
                name="nameValidRequired"
                placeholder="Jane Doe"
                // ref={register({ required: true })}
              />
              {/* {errors.nameValidRequired && (
              <p className="text-warning pt-1">
                <i className="fas fa-exclamation-circle"></i> Your name is
                required
              </p>
            )} */}
            </MDBCol>

            <MDBCol md="4">
              <label htmlFor="defaultFormLoginEmailEx" className="white-text">
                Your email
              </label>
              <input
                type="email"
                id="defaultFormLoginEmailEx"
                className="form-control"
                name="emailValidRequired"
                placeholder="helloworld@gmail.com"
                // ref={register({ required: true })}
              />
              {/* {errors.emailValidRequired && (
              <p className="text-warning pt-1">
                <i className="fas fa-exclamation-circle"></i> Your email address
                is required
              </p>
            )} */}
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBColS md="4">
              <h5 className="mx-5 text-white">
                I will get back to you within 2 days.
              </h5>
              <br />
            </MDBColS>
            <MDBCol md="8">
              <label
                htmlFor="defaultFormLoginEmailEx"
                className="white-text pt-2"
              >
                Select the requirements
              </label>
              <select className="browser-default custom-select">
                <option>How Can I help you?</option>
                <option value="1">Design</option>
                <option value="2">Coding</option>
                <option value="3">Other</option>
              </select>
            </MDBCol>
          </MDBRow>

          <MDBRow>
            <MDBCol md="4"></MDBCol>
            <MDBCol md="8">
              <label
                htmlFor="defaultFormLoginEmailEx"
                className="white-text pt-2"
              >
                Message
              </label>
              <textarea
                className="form-control"
                id="exampleFormControlTextarea1"
                rows="4"
                name="textValidRequired"
                placeholder="Say Hi!"
                // ref={register({ required: true })}
              />
              {/* {errors.textValidRequired && (
              <p className="text-warning pt-1">
                <i className="fas fa-exclamation-circle"></i> Please fill in the
                form
              </p>
            )} */}
            </MDBCol>
          </MDBRow>

          <MDBRow>
            <MDBCol md="4"></MDBCol>
            <MDBCol md="8">
              <Div>
                <Input type="submit" value="SEND" />

                {/* <h3
                className={classnames({
                  // case insensitive comparison
                  "text-white mt-4 animate__animated animate__fadeIn animate__slower":
                    sendStatus.toUpperCase() === "OK",
                  "text-danger mt-2": sendStatus.toUpperCase() !== "OK",
                })}
              >
                <i class="far fa-check-circle"></i>
                {sendStatus === ""
                  ? ""
                  : sendStatus.toUpperCase() === "OK"
                  ? "Your message was sent successfully."
                  : "Error Occurred"}
              </h3> */}
              </Div>
            </MDBCol>
          </MDBRow>
        </FormS>
      </MDBContainer>
    </>
  );
};

export default Contact;

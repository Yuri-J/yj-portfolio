import React, { useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import { Link } from "react-scroll";

import {
  MDBContainer,
  MDBNavbar,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBCollapse,
  MDBIcon,
  MDBNavbarToggler,
} from "mdbreact";

const NavBar = (props) => {
  const { children } = props;

  const [isOpen, setIsOpen] = useState(false);
  const toggleCollapse = () => setIsOpen(!isOpen);

  return (
    <>
      <Router>
        <MDBNavbar
          color=""
          fixed="top"
          dark
          expand="md"
          scrolling
          transparent
          style={{ backgroundColor: "#ba7967" }}
        >
          <MDBNavbarToggler onClick={toggleCollapse} />

          <MDBCollapse id="navbarCollapse3" isOpen={isOpen} navbar>
            <MDBNavbarNav right>
              <MDBNavItem>
                <MDBNavLink to="/yj-portfolio/">HOME</MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink to="/">
                  <Link
                    to="about"
                    spy={true}
                    smooth={true}
                    offset={-120}
                    duration={500}
                  >
                    ABOUT
                  </Link>
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink to="/">
                  <Link
                    to="portfolio"
                    spy={true}
                    smooth={true}
                    offset={-80}
                    duration={500}
                  >
                    PORTFOLIO
                  </Link>
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink to="/">
                  <Link
                    to="skills"
                    spy={true}
                    smooth={true}
                    offset={-80}
                    duration={500}
                  >
                    SKILLS
                  </Link>
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink to="/">
                  <Link
                    to="contact"
                    spy={true}
                    smooth={true}
                    offset={-80}
                    duration={500}
                  >
                    CONTACT
                  </Link>
                </MDBNavLink>
              </MDBNavItem>
            </MDBNavbarNav>
            <MDBNavbarNav right>
              <MDBNavItem>
                <MDBNavLink className="waves-effect waves-light" to="#!">
                  <MDBIcon fab icon="github" size="lg" />
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink className="waves-effect waves-light" to="#!">
                  <MDBIcon fab icon="linkedin" size="lg" />
                </MDBNavLink>
              </MDBNavItem>
            </MDBNavbarNav>
          </MDBCollapse>
        </MDBNavbar>

        <MDBContainer fluid className="mx-0 px-0">
          {children}
        </MDBContainer>
      </Router>
    </>
  );
};

const navBarWithSwitch = (CustomSwitch) => (
  <>
    <NavBar>
      <CustomSwitch />
    </NavBar>
  </>
);

export default navBarWithSwitch;

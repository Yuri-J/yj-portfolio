import React from "react";

import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import PortlaitL from "../assets/portlait-l.jpg";

const About = () => {
  return (
    <>
      <MDBContainer id="about" fluid>
        <MDBContainer>
          <MDBRow style={{ position: "relative", bottom: "5.2rem" }}>
            <MDBCol lg="6" md="6" sm="12">
              <img
                src={PortlaitL}
                className="mx-auto img-fluid w-100"
                style={{ boxShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px" }}
                alt=""
              />
            </MDBCol>
            <MDBCol
              lg="6"
              md="6"
              sm="12"
              className="d-flex flex-column justify-content-center"
            >
              <h2 className="h1-responsive font-weight-bold pt-3">ABOUT ME</h2>
              <p>
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate.
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </MDBContainer>
    </>
  );
};

export default About;

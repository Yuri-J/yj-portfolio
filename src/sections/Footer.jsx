import React from "react";
import {
  MDBIcon,
  MDBBtn,
  MDBCol,
  MDBRow,
  MDBAnimation,
  MDBContainer,
} from "mdbreact";

import { animateScroll } from "react-scroll";
import styled from "styled-components";

import upSVG from "../assets/up.svg";
import BG from "../assets/bg-skill.jpg";

const MDBContainerS = styled(MDBContainer)`
  background: url(${BG});
  background-size: cover;
  text-align: center;
`;
const P = styled.p`
  // color: white;
`;

const Footer = () => {
  return (
    <>
      <MDBContainerS fluid>
        <MDBRow className="text-center pt-5">
          <MDBCol>
            <MDBBtn
              social="li"
              color="blue darken-2"
              href="https://www.linkedin.com/in/yurika-ji/"
              target="_blank"
            >
              <MDBIcon fab icon="linkedin-in" /> Linkedin
            </MDBBtn>
            <MDBBtn
              social="git"
              color="black"
              href="https://github.com/YuriJ2020"
              target="_blank"
            >
              <MDBIcon fab icon="github" /> Github
            </MDBBtn>
          </MDBCol>
        </MDBRow>

        <MDBAnimation type="bounce" reveal>
          <img
            className="pt-5"
            src={upSVG}
            alt="Second slide"
            style={{ width: "2.5rem", cursor: "pointer" }}
            onClick={() => animateScroll.scrollToTop()}
          />
        </MDBAnimation>
        <P className="py-4">
          Copyright &copy;{new Date().getFullYear()}&nbsp; Yuri J
        </P>
      </MDBContainerS>
    </>
  );
};

export default Footer;

import React from "react";
import _ from "lodash";

import { Link } from "react-scroll";

import styled from "styled-components";

import { MDBContainer, MDBView, MDBMask, MDBRow, MDBCol } from "mdbreact";

import Lead1 from "../assets/lead-1.png";
import Lead2 from "../assets/lead-2.png";
import Lead3 from "../assets/lead-3.png";

const MDBContainerS = styled(MDBContainer)`
  background: #5e6f64;
  // padding-bottom: 6rem;

  @media (max-width: 576px) {
    padding-bottom: 4rem;
  }
`;

const MDBRowS = styled(MDBRow)`
  padding-top: 2rem;
`;

const MDBColS = styled(MDBCol)`
  @media (min-width: 1200px) {
    padding: 3rem;
  }

  // @media (max-width: 768px) {
  //   width: 120%;
  // }
`;

const DivBorder = styled.div`
  @media (min-width: 992px) {
    position: relative;
    bottom: 14rem;
    border: 12px solid #dcdcdc;
    box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px,
      rgba(0, 0, 0, 0.22) 0px 15px 12px;
  }

  @media (min-width: 768px) and (max-width: 992px) {
    border: 6px solid #dcdcdc;
    position: relative;
    bottom: 10rem;
    box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px,
      rgba(0, 0, 0, 0.22) 0px 15px 12px;
  }

  @media (max-width: 768px) {
    border: 4px solid #dcdcdc;
    position: relative;
    bottom: 4.6rem;
  }
`;
const H5 = styled.h5`
  @media (max-width: 768px) {
    font-size: 0.6rem;
  }
`;

const leadItems = [
  {
    name: "PORTFOLIO",
    img: Lead1,
    url: "portfolio",
  },
  {
    name: "SKILLS",
    img: Lead2,
    url: "skills",
  },
  {
    name: "CONTACT",
    img: Lead3,
    url: "contact",
  },
];

const Lead = () => {
  return (
    <>
      <MDBContainerS fluid>
        <MDBContainer>
          <MDBRowS>
            {_.map(leadItems, (item, index) => (
              <MDBColS key={index}>
                <DivBorder>
                  <Link
                    to={item.url}
                    spy={true}
                    smooth={true}
                    offset={-80}
                    duration={500}
                  >
                    <MDBView zoom hover>
                      <img src={item.img} className="mx-auto w-100" alt="" />
                      <MDBMask
                        className="flex-center"
                        overlay="white-slight"
                        style={{ cursor: "pointer" }}
                      >
                        <H5 className="white-text font-weight-bold">
                          {item.name}
                        </H5>
                      </MDBMask>
                    </MDBView>
                  </Link>
                </DivBorder>
              </MDBColS>
            ))}
          </MDBRowS>
        </MDBContainer>
      </MDBContainerS>
    </>
  );
};

export default Lead;

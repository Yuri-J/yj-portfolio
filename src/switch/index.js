import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import ScrollAnimation from "react-animate-on-scroll";

import Jumbo from "../sections/Jumbo";
import Lead from "../sections/Lead";
import About from "../sections/About";
import Skills2 from "../sections/Skills2";
import ReactP from "../sections/ReactP";
import VueP from "../sections/VueP";
import HtmlP from "../sections/HtmlP";
import Contact from "../sections/Contact";
import Footer from "../sections/Footer";

import NotFound from "../sections/NotFound";

const CustomSwitch = () => (
  <>
    <Switch>
      <Route exact path="/yj-portfolio/">
        <ScrollAnimation animateIn="fadeIn" animateOnce={true}>
          <Jumbo />
          <Lead />
          <About />
          <Skills2 />
          <ReactP />
          <VueP />
          <HtmlP />
          <Contact />
          <Footer />
        </ScrollAnimation>
      </Route>

      <Route exact path="/yj-portfolio/notfound">
        <div style={{ paddingTop: "9rem" }}>
          <NotFound />
        </div>
      </Route>
      <Route path="/yj-portfolio/">
        <Redirect to="/notfound" />
      </Route>
    </Switch>
  </>
);

export default CustomSwitch;
